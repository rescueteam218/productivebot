#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vk_api
import time
import telebot
import config
import vk_config

vk = vk_api.VkApi(token=vk_config.AccessToken)
conversation_id = vk_config.conversation_id
conversation_id += 2000000000
WEEK = 60 * 60 * 24 * 7
SLEEP_TIME = 60

def get_last_message_id():
    return vk.method('messages.getHistory', values={
        'peer_id':conversation_id,
        'count':1,
        'rev':0
    })['items'][0]["id"]

# Загружаем не более n сообщений, полученных после last_msg
def get_messages(last_msg, n):
    return vk.method('messages.getHistory', values={
        'peer_id':conversation_id,
        'count':n,
        'offset':-n,
        'start_message_id':last_msg
    })['items']
   

# Загружаем информацию о пользователе по его id. Кэшируем данные в словаре users
def get_user_info(user_id):
    if user_id in get_user_info.users:
        if (int(time.time()) - get_user_info.users[user_id]['update_time'] < WEEK):
            return users[user_id]
    get_user_info.users[user_id] = vk.method('users.get', values={'user_ids':[user_id]})[0]
    get_user_info.users[user_id]['update_time'] = int(time.time())
    return users[user_id]
get_user_info.users = {}


def get_user_name(user_id):
    user_info = get_user_info(user_id)
    return "{} {}".format(user_info['first_name'], user_info['last_name'])

# Преобразуем сообщение в читаемую строку.
# Второй параметр - уровень вложенности (для пересланных сообщений)
SEP = '  ' # Разделитель, составляющий отступы для пересланных сообщений
def message_to_str(msg, n = 0):
    res  = SEP * n + str(get_user_name(msg['user_id'])) + '\n'
    res += SEP * n + time.ctime(msg['date']) + '\n'
    body = msg['body']
    body = '\n'.join([SEP * n + string for string in body.split('\n')])
    res += SEP * n + body + '\n'
    if 'fwd_messages' in msg:
        res += SEP * (n+1) + '///Forward///\n'
        for fwd_msg in msg['fwd_messages']:
            res += message_to_str(fwd_msg, n+1)
        res += SEP * (n+1) + '/////////////\n'
    return res + '\n'

# Подключаем Telegram-бота
CHANNEL_NAME = '@produbeta17'
bot = telebot.TeleBot(config.token)

LAST_MESSAGE_FILENAME = 'last_msg'
# Раз в SLEEP_TIME проверяем на наличие новых сообщений
while True:
    try:
        # В файле last_msg храним id последнего отправленного сообщения
        try:
            with open(LAST_MESSAGE_FILENAME) as f_last:
                last_msg = int(f_last.read())
        except:
            # В случае первого запуска начинаем писать сообщения
            # с последнего
            new_last_id = get_last_message_id()
            last_msg = new_last_id  
            print("Inited with id = {}".format(new_last_id))
        for msg in get_messages(last_msg, 20)[::-1]:
            if (msg['id'] <= last_msg):
                continue     
            bot.send_message(CHANNEL_NAME, message_to_str(msg))
            print('Send: {}'.format(msg['id']))
            last_msg = msg['id']
            with open(LAST_MESSAGE_FILENAME, 'w') as f_last:
                f_last.write(str(last_msg))
            time.sleep(1) # Спим секунду во избежание ошибок (на всякий случай)
    except Exception as ex:
        print(ex)
    time.sleep(SLEEP_TIME)
